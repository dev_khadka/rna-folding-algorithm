"use strict";

(function(){

	const basePairs ={"A":"U","U":"A","C":"G","G":"C"};
	const optTable = [];
	const secondaryStructure=[];
	var possiblePairs;
	exports.fold=function(rnaSequence){
		possiblePairs = getPossiblePairs(rnaSequence);

		for(var i=5; i<rnaSequence.length;i++){
			getOpt(0,i);
		}
	console.log(readOptTable(0,rnaSequence.length-1));

	}

	function getOpt(t,m){
		if(m-t<5){
			return 0;
		}
		else if(readOptTable(t,m)){
			return readOptValue(t,m);
		}
		else{
			var retOpt = getOpt(t,m-1);
			var p=-1
			for(var i=t;i<m-4;i++){
				if(possiblePairs[i] && possiblePairs[i][m]){
					var tempOpt = getOpt(t,i-1) + getOpt(i+1, m-1) +1;
					if(retOpt<tempOpt){
						retOpt=tempOpt;
						p=i;
					}
				}
				
			}
			setOptTable(t,m,p,retOpt);
			return retOpt;
		}
		
	}

	function readOptValue(t,m){
		var val = readOptTable(t,m);
		return val[0];
	}
	
	function readOptTable(t,m){
		if(t<m-4){
			if(optTable[t] && optTable[t][m]){
				return optTable[t][m];
			}
			else{
				return null;
			}

		}
		return [0,[]];
		
	}

	function setOptTable(t,m,p, val){
		if(!optTable[t]){
			optTable[t]=[];
		}

		if(p==-1){
			optTable[t][m]=readOptTable(t,m-1);
		}
		else{
			var pairs = readOptTable(t,p-1)[1];
			pairs=pairs.concat([[p,m]]);
			pairs=pairs.concat(readOptTable(p+1,m-1)[1]);
			optTable[t][m]=[val,pairs];
		}
	}

	function getPossiblePairs(seq){
		var pairs = [];
		for (var i = 0; i < seq.length-5; i++) {
			var cpair = basePairs[seq[i]];
			for (var j=i+4;j<seq.length;j++){
				if(seq[j]==cpair){
					if(!pairs[i]){
						pairs[i]=[];
					}
					pairs[i][j]=true;
				}
			}
			

		}
		return pairs;
	}
})()






